const BotClient = require('./bot_api/bot_api')
const config = require('./config.json')
const crypto = require('crypto')
const base64url = require('base64url')
let db, bot

let generateSecret = () => crypto.randomBytes(16).toString('hex')

async function getDBKey(key) {
    try {
        return await db.get(key)
    } catch (e) {
        return null
    }
}

function getDBKeys(chkfunc) {
    return new Promise((rs, rj) => {
        let result = new Map()
        db.createReadStream()
            .on('data', function (data) {
                if (chkfunc(data))
                    result.set(data.key, data.value)
            })
            .on('error', function (err) {
                rj(err)
            })
            .on('end', function () {
                rs(result)
            })
    })
}

async function help(msg) {
    let help_txt = ''
    help_txt += '/setname <特工名> - 设置特工名\n'
    help_txt += '/lsshare - 查询位置分享对象\n'
    help_txt += '/addshare <UID> - 添加位置分享者\n'
    help_txt += '/delshare <UID> - 移除位置分享者\n'
    help_txt += '/clrshare - 清空位置分享者列表\n'
    help_txt += '/whoshared - 谁与您分享了他们的位置\n'
    help_txt += '/joingroup <共享密码> - 加入位置分享组\n'
    help_txt += '/partgroup <共享密码> - 离开位置分享组\n'
    help_txt += '/lsgroup - 查询已加入的位置分享组\n'
    help_txt += '/whoingroup - 查询位置分享组成员\n'
    help_txt += '/id - 获取自己的 UID\n'
    help_txt += '/revoketoken - 撤销并生成新的安全密码\n'
    help_txt += '/getplugin - 获取用于 IITC 的插件\n'
    help_txt += '/owntracks - 获取用于 OwnTracks 的配置文件\n'
    help_txt += '/notificationon - 开启分享通知\n'
    help_txt += '/notificationoff - 关闭分享通知(默认)\n\n'

    help_txt += '与机器人分享您的实时位置信息，它将呈现在您共享的成员的 Intel 上。通常您无需担心位置偏移。\n'
    help_txt += '注意：由接收方发送 ID 给发送方添加，注意方向。'
    help_txt += '组共享密码为加入分享组的唯一凭证，请注意保密和易辨认。在分享组中，每个人均可以看到彼此的位置。\n'
    help_txt += '组共享密码只可由 1 - 32 位 字母/数字/!@#%&+ 组成，大小写敏感。\n'
    help_txt += '更多问题请加入用户群: https://t.me/joinchat/BMh6Q0TPRIPErAWVwtNFkw'

    return bot.sendMessage(msg.from.id, help_txt, {
        reply_to_message_id: msg.message_id
    })
}

async function setName(msg) {
    let name = msg.text.match(/\/setname ([0-9a-zA-Z]+)/i)
    if (!name) return bot.sendMessage(msg.from.id, '无法识别的特工名。', {
        reply_to_message_id: msg.message_id
    })
    name = name[1]
    if (!name) return bot.sendMessage(msg.from.id, '无法识别的特工名。', {
        reply_to_message_id: msg.message_id
    })
    await db.put(`${msg.from.id}:name`, name)
    return bot.sendMessage(msg.from.id, `特工名成功更新。\n\n您好，${name} 特工。`, {
        reply_to_message_id: msg.message_id
    })
}

async function listShare(msg) {
    let agent_name = await getDBKey(`${msg.from.id}:name`)
    if (!agent_name) {
        return await bot.sendMessage(msg.from.id, '使用前，请先设置特工名。', {
            reply_to_message_id: msg.message_id
        })
    }
    let value = await getDBKey(`${msg.from.id}:sharing_peer`)
    if (!value) return bot.sendMessage(msg.from.id, '当前您暂未分享位置给任何人。', {
        reply_to_message_id: msg.message_id
    })
    value = value.split('\n')
    value = value.filter(v => !!v)
    let value2 = []
    for (let v of value) {
        let agent_name = await getDBKey(`${v}:name`)
        value2.push(`[${v}](tg://user?id=${v}) (\`${agent_name}\`)`)
    }
    value = value2
    return bot.sendMessage(msg.from.id, '您将您的位置分享给了：\n - ' + value.join('\n - '), {
        reply_to_message_id: msg.message_id,
        parse_mode: 'Markdown'
    })
}

async function addShare(msg) {
    let my_name = await getDBKey(`${msg.from.id}:name`)
    if (!my_name) {
        return await bot.sendMessage(msg.from.id, '使用前，请先设置特工名。', {
            reply_to_message_id: msg.message_id
        })
    }
    var peer
    if (msg.text.match(/^\/start/)) {
        if (!msg.text.match(/^\/start addshare-[0-9]+/i)) return bot.sendMessage(msg.from.id, '您输入了无效的 UID。', {
            reply_to_message_id: msg.message_id
        })
        peer = msg.text.match(/^\/start addshare-([0-9]+)/i)[1]
    } else {
        if (!msg.text.match(/^\/addshare [0-9]+/i)) return bot.sendMessage(msg.from.id, '您输入了无效的 UID。', {
            reply_to_message_id: msg.message_id
        })
        peer = msg.text.match(/^\/addshare ([0-9]+)/i)[1]
    }
    if (!peer || parseInt(peer) == msg.from.id) return bot.sendMessage(msg.from.id, '您输入了无效的 UID。', {
        reply_to_message_id: msg.message_id
    })
    let agent_name = await getDBKey(`${peer}:name`)
    if (!agent_name) return bot.sendMessage(msg.from.id, `用户 ${peer} 尚未在系统中注册特工名。`, {
        reply_to_message_id: msg.message_id,
        parse_mode: 'Markdown'
    })
    let peers = await getDBKey(`${msg.from.id}:sharing_peer`)
    if (!peers) peers = ''
    peers = peers.split('\n')
    peers = peers.filter(v => !!v)
    peers.push(peer)
    peers = peers.join('\n')
    await db.put(`${msg.from.id}:sharing_peer`, peers)
    let notification = await getDBKey(`${peer}:notification`)
    if (notification) {
        await bot.sendMessage(msg.from.id, `特工 \`${my_name}\` (\`${msg.from.id}\`) 现在开始与您共享位置信息。\n\n使用 /notificationoff 关闭此通知。`, {
            parse_mode: 'Markdown'
        })
    }
    return bot.sendMessage(msg.from.id, `您已成功与 \`${peer}\` (${agent_name}) 共享您的位置。`, {
        reply_to_message_id: msg.message_id,
        parse_mode: 'Markdown'
    })
}

async function delShare(msg) {
    let agent_name = await getDBKey(`${msg.from.id}:name`)
    if (!agent_name) {
        return await bot.sendMessage(msg.from.id, '使用前，请先设置特工名。', {
            reply_to_message_id: msg.message_id
        })
    }
    var peer
    if (!msg.text.match(/^\/delshare [0-9]+/i)) return bot.sendMessage(msg.from.id, '您输入了无效的 UID。', {
        reply_to_message_id: msg.message_id
    })
    peer = msg.text.match(/^\/delshare ([0-9]+)/i)[1]
    if (!peer) return bot.sendMessage(msg.from.id, '您输入了无效的 UID。', {
        reply_to_message_id: msg.message_id
    })
    let peers = await getDBKey(`${msg.from.id}:sharing_peer`)
    if (!peers) peers = ''
    peers = peers.split('\n')
    peers = peers.filter(v => !!v)
    peers = peers.filter(n => n != peer)
    peers = peers.join('\n')
    await db.put(`${msg.from.id}:sharing_peer`, peers)
    return bot.sendMessage(msg.from.id, `您已成功移除与 \`${peer}\` 的共享权限。`, {
        reply_to_message_id: msg.message_id,
        parse_mode: 'Markdown'
    })
}

async function clearShare(msg) {
    let agent_name = await getDBKey(`${msg.from.id}:name`)
    if (!agent_name) {
        return await bot.sendMessage(msg.from.id, '使用前，请先设置特工名。', {
            reply_to_message_id: msg.message_id
        })
    }
    await db.del(`${msg.from.id}:sharing_peer`)
    return bot.sendMessage(msg.from.id, `您已成功清空共享列表。`, {
        reply_to_message_id: msg.message_id,
        parse_mode: 'Markdown'
    })
}

async function whoShared(msg) {
    let agent_name = await getDBKey(`${msg.from.id}:name`)
    if (!agent_name) {
        return await bot.sendMessage(msg.from.id, '使用前，请先设置特工名。', {
            reply_to_message_id: msg.message_id
        })
    }
    let shared_user = await getDBKeys(({
        key,
        value
    }) => key.slice(-13) == ':sharing_peer' && value.match(msg.from.id))
    let result = []
    for ([
        key,
        value
    ] of shared_user) {
        let uid = parseInt(key.slice(0, -13))
        let agent_name = await getDBKey(`${uid}:name`)
        result.push(`[${uid}](tg://user?id=${uid}) (\`${agent_name}\`)`)
    }
    result = result.filter(v => !!v)
    if (value.length <= 0) return bot.sendMessage(msg.from.id, '当前没有任何人与您分享他们的位置信息。', {
        reply_to_message_id: msg.message_id
    })
    return bot.sendMessage(msg.from.id, `以下特工正在与您共享他们的位置信息：\n - ${result.join('\n - ')}`, {
        reply_to_message_id: msg.message_id,
        parse_mode: 'Markdown'
    })
}

async function joinGroup(msg) {
    let my_name = await getDBKey(`${msg.from.id}:name`)
    if (!my_name) {
        return await bot.sendMessage(msg.from.id, '使用前，请先设置特工名。', {
            reply_to_message_id: msg.message_id
        })
    }
    var group
    if (msg.text.match(/^\/start/)) {
        if (!msg.text.match(/^\/start joingroup-[0-9a-zA-Z-_]+/i)) return bot.sendMessage(msg.from.id, '您输入了无效的组预共享密码。', {
            reply_to_message_id: msg.message_id
        })
        group = msg.text.match(/^\/start joingroup-([0-9a-zA-Z-_]+)/i)[1]
        group = base64url.decode(group)
    } else {
        if (!msg.text.match(/^\/joingroup [0-9a-zA-Z!@#%&+-]{1,32}/i)) return bot.sendMessage(msg.from.id, '您输入了无效的组预共享密码。', {
            reply_to_message_id: msg.message_id
        })
        group = msg.text.match(/^\/joingroup ([0-9a-zA-Z!@#%&+-]{1,32})/i)[1]
    }
    if (!group) return bot.sendMessage(msg.from.id, '您输入了无效的组预共享密码。', {
        reply_to_message_id: msg.message_id
    })
    let groups = await getDBKey(`${msg.from.id}:groups`)
    if (!groups) groups = ''
    groups = groups.split('\n')
    groups = groups.filter(v => !!v)
    if (groups.includes(group)) {
        return bot.sendMessage(msg.from.id, '您已加入过此分享组。', {
            reply_to_message_id: msg.message_id
        })
    }
    groups.push(group)
    groups = groups.join('\n')
    await db.put(`${msg.from.id}:groups`, groups)
    let me = await bot.getMe()
    return bot.sendMessage(msg.from.id, `您已成功加入位置分享组。\n\n预共享密码（可点击复制）：\`${group}\`\n邀请链接：${`https://t.me/${me.username}?start=joingroup-${base64url.encode(group)}`}`, {
        reply_to_message_id: msg.message_id,
        parse_mode: 'Markdown'
    })
}

async function partGroup(msg) {
    let agent_name = await getDBKey(`${msg.from.id}:name`)
    if (!agent_name) {
        return await bot.sendMessage(msg.from.id, '使用前，请先设置特工名。', {
            reply_to_message_id: msg.message_id
        })
    }
    var group
    if (!msg.text.match(/^\/partgroup [0-9a-zA-Z!@#%&+-]{1,32}/i)) return bot.sendMessage(msg.from.id, '您输入了无效的组预共享密码。', {
        reply_to_message_id: msg.message_id
    })
    group = msg.text.match(/^\/partgroup ([0-9a-zA-Z!@#%&+-]{1,32})/i)[1]
    if (!group) return bot.sendMessage(msg.from.id, '您输入了无效的组预共享密码。', {
        reply_to_message_id: msg.message_id
    })
    let groups = await getDBKey(`${msg.from.id}:groups`)
    if (!groups) groups = ''
    groups = groups.split('\n')
    groups = groups.filter(v => !!v)
    groups = groups.filter(n => n != group)
    groups = groups.join('\n')
    await db.put(`${msg.from.id}:groups`, groups)
    return bot.sendMessage(msg.from.id, `您已成功离开位置分享组 \`${group}\`。`, {
        reply_to_message_id: msg.message_id,
        parse_mode: 'Markdown'
    })
}

async function listGroup(msg) {
    let agent_name = await getDBKey(`${msg.from.id}:name`)
    if (!agent_name) {
        return await bot.sendMessage(msg.from.id, '使用前，请先设置特工名。')
    }
    let value = await getDBKey(`${msg.from.id}:groups`)
    if (!value) return bot.sendMessage(msg.from.id, '当前您暂未加入任何位置分享组。', {
        reply_to_message_id: msg.message_id
    })
    value = value.split('\n')
    value = value.filter(v => !!v)
    if (value.length <= 0) return bot.sendMessage(msg.from.id, '当前您暂未加入任何位置分享组。', {
        reply_to_message_id: msg.message_id
    })
    value = value.map(n => '`' + n + '`')
    return bot.sendMessage(msg.from.id, '当前您加入了以下位置分享组：\n - ' + value.join('\n - '), {
        reply_to_message_id: msg.message_id,
        parse_mode: 'Markdown'
    })
}

async function whoInGroup(msg) {
    let agent_name = await getDBKey(`${msg.from.id}:name`)
    if (!agent_name) {
        return await bot.sendMessage(msg.from.id, '使用前，请先设置特工名。', {
            reply_to_message_id: msg.message_id
        })
    }
    var group
    if (!msg.text.match(/^\/whoingroup [0-9a-zA-Z!@#%&+-]{1,32}/i)) return bot.sendMessage(msg.from.id, '您输入了无效的组预共享密码。', {
        reply_to_message_id: msg.message_id
    })
    group = msg.text.match(/^\/whoingroup ([0-9a-zA-Z!@#%&+-]{1,32})/i)[1]
    if (!group) return bot.sendMessage(msg.from.id, '您输入了无效的组预共享密码。', {
        reply_to_message_id: msg.message_id
    })
    let all_group_peers = new Set()
    let group_peers = await getDBKeys(({
        key,
        value
    }) => key.slice(-7) == ':groups' && value.match(group))
    for ([k, v] of group_peers) {
        let uid = parseInt(k.slice(0, -7))
        all_group_peers.add(uid)
    }
    if (all_group_peers.size <= 0) return bot.sendMessage(msg.from.id, '此位置分享组中无任何成员。', {
        reply_to_message_id: msg.message_id
    })
    let result = []
    for (uid of all_group_peers) {
        let agent_name = await getDBKey(`${uid}:name`)
        result.push(`[${uid}](tg://user?id=${uid}) (\`${agent_name}\`)`)
    }
    return bot.sendMessage(msg.from.id, '当前以下特工加入了此位置分享组：\n - ' + result.join('\n - '), {
        reply_to_message_id: msg.message_id,
        parse_mode: 'Markdown'
    })
}

async function getId(msg) {
    let text = ''
    text += `您的 ID: \`${msg.from.id}\`\n`
    let me = await bot.getMe()
    text += `快速共享链接: https://t.me/${me.username}?start=addshare-${msg.from.id}\n`
    return bot.sendMessage(msg.from.id, text, {
        reply_to_message_id: msg.message_id,
        parse_mode: 'Markdown'
    })
}

async function revokeToken(msg) {
    let agent_name = await getDBKey(`${msg.from.id}:name`)
    if (!agent_name) {
        return await bot.sendMessage(msg.from.id, '使用前，请先设置特工名。')
    }
    let new_token = generateSecret()
    db.batch()
        .put(`${msg.from.id}:privkey`, new_token)
        .del(`${msg.from.id}:pos`)
        .del(`${msg.from.id}:last_updated`)
        .del(`${msg.from.id}:sharing_peer`)
        .write()
    return bot.sendMessage(msg.from.id, '您的安全密码已重置，请卸载并重新安装插件。同时，您存储在服务器上的位置信息与分享列表已清空。', {
        reply_to_message_id: msg.message_id
    })
}

async function getPlugin(msg) {
    let priv = await getDBKey(`${msg.from.id}:privkey`)
    if (!priv) return bot.sendMessage(msg.from.id, '请先使用 /revoketoken 重置一个安全密码。', {
        reply_to_message_id: msg.message_id
    })
    return bot.sendMessage(msg.from.id, `插件下载链接如下：\n\nhttps://agent-realtime-tracker.jsw3286.eu.org/realtime-tracker.user.js?priv=${priv}\n\n请确保 Ingress Intel 所登录的账号与您绑定的特工名一致。`, {
        reply_to_message_id: msg.message_id
    })
}

async function getOwnTracksCfg(msg) {
    let agent_name = await getDBKey(`${msg.from.id}:name`)
    if (!agent_name) {
        return await bot.sendMessage(msg.from.id, '使用前，请先设置特工名。')
    }
    let priv = await getDBKey(`${msg.from.id}:privkey`)
    if (!priv) return bot.sendMessage(msg.from.id, '请先使用 /revoketoken 重置一个安全密码。', {
        reply_to_message_id: msg.message_id
    })
    let otrc = {
        '_type': 'configuration',
        auth: true,
        username: msg.from.id.toString(),
        password: priv,
        mode: 3,
        locatorAccuracyBackground: 0,
        locatorDisplacement: 15,
        ignoreStaleLocations: 1,
        locatorInterval: 60,
        monitoring: 2,
        url: 'https://agent-realtime-tracker.jsw3286.eu.org/owntracks',
        beaconMode: 2,
        cp: false,
        httpSchedulerConsiderStrategyDirect: true,
        notification: true,
        notificationLocation: true,
        policymode: 0,
        remoteConfiguration: true
    }
    let otrc_b = Buffer.from(JSON.stringify(otrc), 'utf8')
    return bot.sendDocument(msg.from.id, otrc_b, { caption: '请下载并安装此配置文件。' }, { filename: `${agent_name}.otrc` })
}

async function changeNotification(msg, state) {
    let agent_name = await getDBKey(`${msg.from.id}:name`)
    if (!agent_name) {
        return await bot.sendMessage(msg.from.id, '使用前，请先设置特工名。', {
            reply_to_message_id: msg.message_id
        })
    }
    await db.put(`${msg.from.id}:notification`, state)
    return bot.sendMessage(msg.from.id, `您已成功 ${state ? '开启' : '关闭'} 分享通知。`, {
        reply_to_message_id: msg.message_id
    })
}

async function processLocationUpdate(msg, is_first) {
    if (is_first) {
        let agent_name = await getDBKey(`${msg.from.id}:name`)
        if (!agent_name) {
            return await bot.sendMessage(msg.from.id, '使用前，请先设置特工名。', {
                reply_to_message_id: msg.message_id
            })
        }
        if (!msg.location.live_period) {
            return await bot.sendMessage(msg.from.id, '请使用实时位置分享（红色选项）而非发送当前位置。', {
                reply_to_message_id: msg.message_id
            })
        }
        await bot.sendMessage(msg.from.id, '我们已经收到了您的位置并正在持续记录。', {
            reply_to_message_id: msg.message_id
        })
    } else {
        if (!msg.location.expires_in) {
            await bot.sendMessage(msg.from.id, '您的位置分享会话已过期。', {
                reply_to_message_id: msg.message_id
            })
        }
    }
    return db.batch()
        .put(`${msg.from.id}:pos`, `${msg.location.latitude},${msg.location.longitude}`)
        .put(`${msg.from.id}:last_updated`, msg.edit_date * 1000 || msg.date * 1000)
        .write()
}

async function processIncomeMsg(msg) {
    if (msg.chat.id < 0) return
    if (msg.location) return processLocationUpdate(msg, true)
    if (!msg.text) return
    if (msg.text.slice(0, 1) != '/') return

    let cmd = msg.text.match(/^\/([a-zA-Z]+)/)[1]
    cmd = cmd.toLowerCase()
    if (cmd == 'start') {
        if (msg.text == '/start') return help(msg)
        let subcmd = msg.text.match(/^\/start ([a-zA-Z]+)/i)[1]
        switch (subcmd) {
            case 'addshare':
                return addShare(msg)
            case 'joingroup':
                return joinGroup(msg)
        }
    }
    switch (cmd) {
        case 'help':
            return help(msg)
        case 'setname':
            return setName(msg)
        case 'lsshare':
            return listShare(msg)
        case 'addshare':
            return addShare(msg)
        case 'delshare':
            return delShare(msg)
        case 'clrshare':
            return clearShare(msg)
        case 'whoshared':
            return whoShared(msg)
        case 'joingroup':
            return joinGroup(msg)
        case 'partgroup':
            return partGroup(msg)
        case 'lsgroup':
            return listGroup(msg)
        case 'whoingroup':
            return whoInGroup(msg)
        case 'id':
            return getId(msg)
        case 'revoketoken':
            return revokeToken(msg)
        case 'getplugin':
            return getPlugin(msg)
        case 'owntracks':
            return getOwnTracksCfg(msg)
        case 'notificationon':
            return changeNotification(msg, true)
        case 'notificationoff':
            return changeNotification(msg, false)
        default:
            return bot.sendMessage(msg.from.id, '命令无法识别。您想做什么？')
    }
}

async function processEditMsg(msg) {
    if (msg.chat.id < 0) return
    if (msg.location) return processLocationUpdate(msg, false)
}

async function init(_db) {
    bot = new BotClient(config.api_id, config.api_hash, config.bot_token)
    db = _db
    bot.on('message', processIncomeMsg)
    bot.on('edited_message', processEditMsg)
}

module.exports = init
