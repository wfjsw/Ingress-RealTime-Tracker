const level = require('level')
const express = require('express')
const cors = require('cors')
const bodyparser = require('body-parser')
const basicauth = require('basic-auth')
const fs = require('fs')

const metajs = fs.readFileSync('./realtime-tracker.meta.js', {
    encoding: 'utf8'
})
const userjs = fs.readFileSync('./realtime-tracker.user.js', {
    encoding: 'utf8'
})

const db = level('./data/')

require('./bot')(db)

const app = express()

app.set('x-powered-by', false)

app.use(cors({
    origin: ['http://ingress.com',
        'https://ingress.com',
        'http://www.ingress.com',
        'https://www.ingress.com'
    ]
}))

app.use(bodyparser.json())
app.use(bodyparser.urlencoded({
    extended: false
}))

async function getDBKey(key) {
    try {
        return await db.get(key)
    } catch (e) {
        return null
    }
}

function getDBKeys(chkfunc) {
    return new Promise((rs, rj) => {
        let result = new Map()
        db.createReadStream()
            .on('data', function (data) {
                if (chkfunc(data))
                    result.set(data.key, data.value)
            })
            .on('error', function (err) {
                rj(err)
            })
            .on('end', function () {
                rs(result)
            })
    })
}

async function auth(req, res, next) {
    if (!req.body.agent_name || !req.body.priv_key) {
        return res.sendStatus(403)
    }
    let match_user = await getDBKeys(({
        key,
        value
    }) => value.toLowerCase() == req.body.priv_key.toLowerCase() && key.slice(-8) == ':privkey')
    if (match_user.size == 0) return res.sendStatus(403)
    if (match_user.size > 1) return res.sendStatus(500)
    let user = parseInt([...match_user.keys()][0].slice(0, -8))
    let agent_name = await getDBKey(`${user}:name`)
    if (agent_name.toLowerCase() != req.body.agent_name.toLowerCase()) return res.sendStatus(403)
    res.locals.user = user
    return next()
}

async function query(req, res, next) {
    let shared_user = await getDBKeys(({
        key,
        value
    }) => key.slice(-13) == ':sharing_peer' && value.match(res.locals.user))
    let own_pos = await getDBKey(`${res.locals.user}:pos`)
    let own_last_updated = await getDBKey(`${res.locals.user}:last_updated`)
    let own_name = await getDBKey(`${res.locals.user}:name`)
    let existed_result = new Set()
    let result = []
    for ([key, value] of shared_user) {
        let uid = parseInt(key.slice(0, -13))
        if (existed_result.has(uid)) continue
        if (res.locals.user == uid) continue
        let agent_name = await getDBKey(`${uid}:name`)
        let pos = await getDBKey(`${uid}:pos`)
        if (!pos) continue
        let [lat, long] = pos.split(',')
        lat = parseFloat(lat)
        long = parseFloat(long)
        let last_updated = await getDBKey(`${uid}:last_updated`)
        last_updated = parseInt(last_updated)
        existed_result.add(uid)
        result.push({
            uid,
            agent_name,
            lat,
            long,
            last_updated
        })
    }
    if (own_last_updated) {
        let [lat, long] = own_pos.split(',')
        lat = parseFloat(lat)
        long = parseFloat(long)
        own_last_updated = parseInt(own_last_updated)
        result.push({
            uid: res.locals.user,
            agent_name: own_name,
            lat,
            long,
            last_updated: own_last_updated
        })
    }
    let sharing_group = await getDBKey(`${res.locals.user}:groups`)
    if (!sharing_group) sharing_group = ''
    sharing_group = sharing_group.split('\n')
    sharing_group = sharing_group.filter(v => !!v)
    if (sharing_group.length > 0) {
        let all_group_peers = new Set()
        for (let g of sharing_group) {
            let group_peers = await getDBKeys(({
                key,
                value
            }) => key.slice(-7) == ':groups' && value.match(g))
            for ([k, v] of group_peers) {
                let uid = parseInt(k.slice(0, -7))
                if (existed_result.has(uid)) continue
                if (res.locals.user == uid) continue
                all_group_peers.add(uid)
            }
        }
        for (uid of all_group_peers) {
            // if (existed_result.has(uid)) continue
            // if (res.locals.user == uid) continue
            let agent_name = await getDBKey(`${uid}:name`)
            let pos = await getDBKey(`${uid}:pos`)
            if (!pos) continue
            let [lat, long] = pos.split(',')
            lat = parseFloat(lat)
            long = parseFloat(long)
            let last_updated = await getDBKey(`${uid}:last_updated`)
            last_updated = parseInt(last_updated)
            existed_result.add(uid)
            result.push({
                uid,
                agent_name,
                lat,
                long,
                last_updated
            })
        }
    }

    return res.json({
        ok: true,
        result
    })
}

async function basicAuthorizer(req, res, next) {
    let auth = req.get('authorization')
    if (!auth) return res.sendStatus(401)
    let {name, pass} = basicauth.parse(auth)
    if (isNaN(name)) return res.sendStatus(401)
    let user_priv = await getDBKey(`${name}:privkey`)
    if (pass.toLowerCase() != user_priv.toLowerCase()) return res.sendStatus(401)
    res.locals.user = parseInt(name)
    return next()
}

async function owntracks(req, res, next) {
    if (req.body['_type'] != 'location') return res.json([])
    // if (req.body.acc > 75) return res.json([])
    await db.batch()
        .put(`${res.locals.user}:pos`, `${req.body.lat},${req.body.lon}`)
        .put(`${res.locals.user}:last_updated`, req.body.tst * 1000)
        .write()
    return res.json([])
    
}

async function getMetaJs(req, res, next) {
    let privkey = req.query.priv || ''
    let data = metajs.replace(/%%%PRIVKEY%%%/g, privkey)
    res.setHeader('Content-Type', 'application/javascript')
    res.setHeader('Cache-Control', 'private')
    return res.send(data)
}

async function getUserJs(req, res, next) {
    let privkey = req.query.priv || ''
    let data = userjs.replace(/%%%PRIVKEY%%%/g, privkey)
    res.setHeader('Content-Type', 'application/javascript')
    res.setHeader('Cache-Control', 'private')
    return res.send(data)
}

app.post('/query', auth, query)

app.post('/owntracks', basicAuthorizer, owntracks)

app.get('/realtime-tracker.user.js', getUserJs)
app.get('/realtime-tracker.meta.js', getMetaJs)

app.use((req, res) => res.sendStatus(403))

app.listen(28006)
