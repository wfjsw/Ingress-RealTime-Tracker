// ==UserScript==
// @id             iitc-plugin-realtime-tracker@wftoolkit
// @name           IITC Plugin: Player Realtime tracker
// @category       Layer
// @version        0.11.1.20170108.21735
// @namespace      https://github.com/jonatkins/ingress-intel-total-conversion
// @updateURL      https://agent-realtime-tracker.jsw3286.eu.org/realtime-tracker.meta.js?priv=%%%PRIVKEY%%%
// @downloadURL    https://agent-realtime-tracker.jsw3286.eu.org/realtime-tracker.user.js?priv=%%%PRIVKEY%%%
// @description    [iitc-2017-01-08-021735] Fetch Realtime Player Location from Server.
// @include        https://*.ingress.com/intel*
// @include        http://*.ingress.com/intel*
// @match          https://*.ingress.com/intel*
// @match          http://*.ingress.com/intel*
// @include        https://*.ingress.com/mission/*
// @include        http://*.ingress.com/mission/*
// @match          https://*.ingress.com/mission/*
// @match          http://*.ingress.com/mission/*
// @grant          none
// ==/UserScript==
