// ==UserScript==
// @id             iitc-plugin-realtime-tracker@wftoolkit
// @name           IITC Plugin: Player Realtime tracker
// @category       Layer
// @version        0.11.1.20170108.21735
// @namespace      https://github.com/jonatkins/ingress-intel-total-conversion
// @updateURL      https://agent-realtime-tracker.jsw3286.eu.org/realtime-tracker.meta.js?priv=%%%PRIVKEY%%%
// @downloadURL    https://agent-realtime-tracker.jsw3286.eu.org/realtime-tracker.user.js?priv=%%%PRIVKEY%%%
// @description    [iitc-2017-01-08-021735] Fetch Realtime Player Location from Server.
// @include        https://*.ingress.com/intel*
// @include        http://*.ingress.com/intel*
// @match          https://*.ingress.com/intel*
// @match          http://*.ingress.com/intel*
// @include        https://*.ingress.com/mission/*
// @include        http://*.ingress.com/mission/*
// @match          https://*.ingress.com/mission/*
// @match          http://*.ingress.com/mission/*
// @grant          none
// ==/UserScript==


function wrapper(plugin_info) {
    // ensure plugin framework is there, even if iitc is not yet loaded
    if (typeof window.plugin !== 'function') window.plugin = function () {};

    //PLUGIN AUTHORS: writing a plugin outside of the IITC build environment? if so, delete these lines!!
    //(leaving them in place might break the 'About IITC' page or break update checks)
    // plugin_info.buildName = 'iitc';
    // plugin_info.dateTimeVersion = '20170108.21732';
    // plugin_info.pluginId = 'realtime-tracker';
    //END PLUGIN AUTHORS NOTE



    // PLUGIN START ////////////////////////////////////////////////////////
    window.FOLLOW_GLOBAL_REFRESH_TIMEOUT = true
    window.REALTIME_TRACKER_REQUEST_INTERVAL = 8 * 1000
    window.REALTIME_TRACKER_MAX_TIME = 3 * 60 * 60 * 1000; // in milliseconds
    window.REALTIME_TRACKER_MIN_ZOOM = 9;
    window.REALTIME_TRACKER_MIN_OPACITY = 0.3;
    window.REALTIME_TRACKER_LINE_COLOUR = '#FF00FD';


    // use own namespace for plugin
    window.plugin.realtimeTracker = function () {};
    window.plugin.realtimeTracker.labelLayers = {};
    window.plugin.realtimeTracker.labelLayerGroup = null;
    window.plugin.realtimeTracker.NAME_WIDTH = 80;
    window.plugin.realtimeTracker.NAME_HEIGHT = 23;

    window.plugin.realtimeTracker.setup = function () {
        $('<style>').prop('type', 'text/css').html('.plugin-realtime-tracker-popup a {\n	color: inherit;\n	text-decoration: underline;\n	text-decoration-style: dashed;\n	-moz-text-decoration-style: dashed;\n	-webkit-text-decoration-style: dashed;\n}\n').appendTo('head');

        var iconImage = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAAACXBIWXMAAAsTAAALEwEAmpwYAAAKT2lDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVNnVFPpFj333vRCS4iAlEtvUhUIIFJCi4AUkSYqIQkQSoghodkVUcERRUUEG8igiAOOjoCMFVEsDIoK2AfkIaKOg6OIisr74Xuja9a89+bN/rXXPues852zzwfACAyWSDNRNYAMqUIeEeCDx8TG4eQuQIEKJHAAEAizZCFz/SMBAPh+PDwrIsAHvgABeNMLCADATZvAMByH/w/qQplcAYCEAcB0kThLCIAUAEB6jkKmAEBGAYCdmCZTAKAEAGDLY2LjAFAtAGAnf+bTAICd+Jl7AQBblCEVAaCRACATZYhEAGg7AKzPVopFAFgwABRmS8Q5ANgtADBJV2ZIALC3AMDOEAuyAAgMADBRiIUpAAR7AGDIIyN4AISZABRG8lc88SuuEOcqAAB4mbI8uSQ5RYFbCC1xB1dXLh4ozkkXKxQ2YQJhmkAuwnmZGTKBNA/g88wAAKCRFRHgg/P9eM4Ors7ONo62Dl8t6r8G/yJiYuP+5c+rcEAAAOF0ftH+LC+zGoA7BoBt/qIl7gRoXgugdfeLZrIPQLUAoOnaV/Nw+H48PEWhkLnZ2eXk5NhKxEJbYcpXff5nwl/AV/1s+X48/Pf14L7iJIEyXYFHBPjgwsz0TKUcz5IJhGLc5o9H/LcL//wd0yLESWK5WCoU41EScY5EmozzMqUiiUKSKcUl0v9k4t8s+wM+3zUAsGo+AXuRLahdYwP2SycQWHTA4vcAAPK7b8HUKAgDgGiD4c93/+8//UegJQCAZkmScQAAXkQkLlTKsz/HCAAARKCBKrBBG/TBGCzABhzBBdzBC/xgNoRCJMTCQhBCCmSAHHJgKayCQiiGzbAdKmAv1EAdNMBRaIaTcA4uwlW4Dj1wD/phCJ7BKLyBCQRByAgTYSHaiAFiilgjjggXmYX4IcFIBBKLJCDJiBRRIkuRNUgxUopUIFVIHfI9cgI5h1xGupE7yAAygvyGvEcxlIGyUT3UDLVDuag3GoRGogvQZHQxmo8WoJvQcrQaPYw2oefQq2gP2o8+Q8cwwOgYBzPEbDAuxsNCsTgsCZNjy7EirAyrxhqwVqwDu4n1Y8+xdwQSgUXACTYEd0IgYR5BSFhMWE7YSKggHCQ0EdoJNwkDhFHCJyKTqEu0JroR+cQYYjIxh1hILCPWEo8TLxB7iEPENyQSiUMyJ7mQAkmxpFTSEtJG0m5SI+ksqZs0SBojk8naZGuyBzmULCAryIXkneTD5DPkG+Qh8lsKnWJAcaT4U+IoUspqShnlEOU05QZlmDJBVaOaUt2ooVQRNY9aQq2htlKvUYeoEzR1mjnNgxZJS6WtopXTGmgXaPdpr+h0uhHdlR5Ol9BX0svpR+iX6AP0dwwNhhWDx4hnKBmbGAcYZxl3GK+YTKYZ04sZx1QwNzHrmOeZD5lvVVgqtip8FZHKCpVKlSaVGyovVKmqpqreqgtV81XLVI+pXlN9rkZVM1PjqQnUlqtVqp1Q61MbU2epO6iHqmeob1Q/pH5Z/YkGWcNMw09DpFGgsV/jvMYgC2MZs3gsIWsNq4Z1gTXEJrHN2Xx2KruY/R27iz2qqaE5QzNKM1ezUvOUZj8H45hx+Jx0TgnnKKeX836K3hTvKeIpG6Y0TLkxZVxrqpaXllirSKtRq0frvTau7aedpr1Fu1n7gQ5Bx0onXCdHZ4/OBZ3nU9lT3acKpxZNPTr1ri6qa6UbobtEd79up+6Ynr5egJ5Mb6feeb3n+hx9L/1U/W36p/VHDFgGswwkBtsMzhg8xTVxbzwdL8fb8VFDXcNAQ6VhlWGX4YSRudE8o9VGjUYPjGnGXOMk423GbcajJgYmISZLTepN7ppSTbmmKaY7TDtMx83MzaLN1pk1mz0x1zLnm+eb15vft2BaeFostqi2uGVJsuRaplnutrxuhVo5WaVYVVpds0atna0l1rutu6cRp7lOk06rntZnw7Dxtsm2qbcZsOXYBtuutm22fWFnYhdnt8Wuw+6TvZN9un2N/T0HDYfZDqsdWh1+c7RyFDpWOt6azpzuP33F9JbpL2dYzxDP2DPjthPLKcRpnVOb00dnF2e5c4PziIuJS4LLLpc+Lpsbxt3IveRKdPVxXeF60vWdm7Obwu2o26/uNu5p7ofcn8w0nymeWTNz0MPIQ+BR5dE/C5+VMGvfrH5PQ0+BZ7XnIy9jL5FXrdewt6V3qvdh7xc+9j5yn+M+4zw33jLeWV/MN8C3yLfLT8Nvnl+F30N/I/9k/3r/0QCngCUBZwOJgUGBWwL7+Hp8Ib+OPzrbZfay2e1BjKC5QRVBj4KtguXBrSFoyOyQrSH355jOkc5pDoVQfujW0Adh5mGLw34MJ4WHhVeGP45wiFga0TGXNXfR3ENz30T6RJZE3ptnMU85ry1KNSo+qi5qPNo3ujS6P8YuZlnM1VidWElsSxw5LiquNm5svt/87fOH4p3iC+N7F5gvyF1weaHOwvSFpxapLhIsOpZATIhOOJTwQRAqqBaMJfITdyWOCnnCHcJnIi/RNtGI2ENcKh5O8kgqTXqS7JG8NXkkxTOlLOW5hCepkLxMDUzdmzqeFpp2IG0yPTq9MYOSkZBxQqohTZO2Z+pn5mZ2y6xlhbL+xW6Lty8elQfJa7OQrAVZLQq2QqboVFoo1yoHsmdlV2a/zYnKOZarnivN7cyzytuQN5zvn//tEsIS4ZK2pYZLVy0dWOa9rGo5sjxxedsK4xUFK4ZWBqw8uIq2Km3VT6vtV5eufr0mek1rgV7ByoLBtQFr6wtVCuWFfevc1+1dT1gvWd+1YfqGnRs+FYmKrhTbF5cVf9go3HjlG4dvyr+Z3JS0qavEuWTPZtJm6ebeLZ5bDpaql+aXDm4N2dq0Dd9WtO319kXbL5fNKNu7g7ZDuaO/PLi8ZafJzs07P1SkVPRU+lQ27tLdtWHX+G7R7ht7vPY07NXbW7z3/T7JvttVAVVN1WbVZftJ+7P3P66Jqun4lvttXa1ObXHtxwPSA/0HIw6217nU1R3SPVRSj9Yr60cOxx++/p3vdy0NNg1VjZzG4iNwRHnk6fcJ3/ceDTradox7rOEH0x92HWcdL2pCmvKaRptTmvtbYlu6T8w+0dbq3nr8R9sfD5w0PFl5SvNUyWna6YLTk2fyz4ydlZ19fi753GDborZ752PO32oPb++6EHTh0kX/i+c7vDvOXPK4dPKy2+UTV7hXmq86X23qdOo8/pPTT8e7nLuarrlca7nuer21e2b36RueN87d9L158Rb/1tWeOT3dvfN6b/fF9/XfFt1+cif9zsu72Xcn7q28T7xf9EDtQdlD3YfVP1v+3Njv3H9qwHeg89HcR/cGhYPP/pH1jw9DBY+Zj8uGDYbrnjg+OTniP3L96fynQ89kzyaeF/6i/suuFxYvfvjV69fO0ZjRoZfyl5O/bXyl/erA6xmv28bCxh6+yXgzMV70VvvtwXfcdx3vo98PT+R8IH8o/2j5sfVT0Kf7kxmTk/8EA5jz/GMzLdsAAAAgY0hSTQAAeiUAAICDAAD5/wAAgOkAAHUwAADqYAAAOpgAABdvkl/FRgAAA81JREFUeNrcWs1L22AcLg5hnQfLjtJ8HDKI7IOuG0sRuw4LEYdzA0EQd9mmB/cBGxbEg+YPMMZD3aWIO+w0BgriwT9hZ/GysU0zKF4qc8zSSkKfHbqmyaz2TfLWzr7wu7RJfs+T/L7fNxBowFJVtR2ScBGi3AFR7oAkXFRVtT3wPy5FUdogCSGEEwz4pAhGjoAdvFVTGDkCPikinGAgCSFFUdqaBhxdty+Bj3CnAq4njBwBH+HQdfvS2QLnBgXPoE8SblBoKBFFUdoQTjDgYlHq4C0SsSjCCYa6aYHVgmCuX20Y8GOmdf0qWC1IB7wodSJ85+aZga9I+M5NiFKnzzd/5XJDTYbEpNgrl72Bl4RQU8HbSUhCyH2kaYbZnGZOpBGqHG0GrvlWeq+vF2/eDmFyfgA3+iX/JAauZTKZC/XffjjBeFby8dNr7GZXUSx8x7+rWPiO3ewqPn567Z1EgiFIUh7sfn0zhWIhC9JVLGSxvpny5A+nmZLrDDuvjeLgaKsmyMM88FUH9BxM06xN5OBoC/PaqNuMffLbd/OgleVnpmnmHIB+ZIGZNNDzGGB7q8LFyr/NpMvX2JZpmjksb4y70l3rK4CPcMQPWNDGSqVS3kJhGICSBoS4E3gtEeLlaw3Dur1UKuWxoI0R6+cj3PHI46aqPDjatrTv/wSGXtUH/q8MvQJyh3Zz2nZTxTrqJYhSpwuHnbJ9fkB+4h58ReQnTv9Y35wixmEvM8A+DRPfaI82s0vewVdkbtEWnYysp5AKPikS3fR+47nDYQXZPwFBBnRbLHi/8ZzMD5JilQCp/X/b+2ApUtL+wVdESVcJfNv7QOoH1Qac9LMd5j9bihIj9AgkRuz54zMpHlVV28vNCmltY09SXIweAS4G/PpdJXF3Ok5GQgsGIModRBentIeWgi+79MBX5MtulUBKe0gWieQOcgIvFu5bCnSdPgG7I0/OD5ATIDWhG/2SI/5zfVRNyJEPiMtvLXj+nfjch1F/iSxOIZHFKSSyZpYSs0sUSgl3xVyqgcVcylMx56+czgEPXroH/+Bl+V4a5TSVhmZukbyhmVuk29B4aynfTcAw9h39oZ7721IOHwfeM1z+T3d2oTCMfay8m/DdUlJv6n/9Ptum3v9YxXAxVjEaM1ahMtjS9TUUCzs1ZkE70PW1hg62AoFAIJPJXKAyWrw7HW/KaPHcD3dbYrzeEhscLbHF1BKbfC2xzdoSG921a6dHvO+jBuwj/kyBn3jYo2ucBRftrnvYg4t2o2ucbfphj7rHbVgtaB23YbVgo47b/BkAYs4ngTdeUtUAAAAASUVORK5CYII=';
        var iconImage2x = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGAAAABgCAYAAADimHc4AAAgAElEQVR4Ad3BCZycVYHu4f97zldVXd2dFUIwZGGJbCqiKCowiiKuIDiujDsglxkUd/Q6g6Lo/NwVcUbAfRR1LooKKAp6ryKCCCLiArImhACB7Omlqr7vnPcW3VFiZ+sOSYB5HvEIcsqFXy4+/On/rWJpAu8ZnaYhMMUykxc7TU/5fa872f9+3HsyjxDiEeKsj3+yQb2vdsI5p1ntSbiYF5SaASqIwybcm6upU3zRkYf7rkn7dU5600srHgHEw4x32y+inXrIqQmxN2f3VjU36lUt5o4ijaZDGoDYAMtQggR0hDPQS8dFJpdlve42uWpBHgYNOq9shTuuyTyMiIeBj3/4kz3vPPejUxhsTiXv2kNs1nApKEQmoxI6EqFhKiB2oGhAiOAECpArcBuqAKEpUglFFYgBgozJDmVSWNzyDlrx+Re+atlJ7z+15CEmHiKtOfvW6vU4VZqyI+3pTWJbxB45G+UEzhBkzCgHUAAZnCADioBBAgwCEqAAEXAWOYEEZFCANAihIYJzSmkwFMuWEr0q3Hx9yUNAbGeefUgPVDuBpxJTnWoS0DQpQpEgFIABM8o8QGyceYD4O6IrgyM4gYagrIM7gVgaVR2s1ah2t+78ZZvtSGwnnvfcPlSbidNUnAPCgKEQjhAAJ8iAAqPMKDNKbJp5gBghMcIGCWRGpAyhgJDBGTIBZNBylO7WwouH2Q7ENlbN2b8RY+8smDwV1wMCnMEGMcpi08wosWkGxKaZUWKEDKZLdAkJKBNp1WroLNKdv+2wDYltZHjvw0PP8hUzUZpZ9TeLWE3Oog62wCCBzfZnRom/kcCABLiro8RQCFWrCo3G3br98nvYRsQ28LkT39J70rd/PpfQ6KWvLooG5Dq4AMwDDIiHlgExSiDAFbgFtEVZQNbAl1+5/23HffaMDluZ2MqqPZ6ws8LKWSHNF9RMlUQoINMlHtYkwOAMSiAgG6uU4p3VitbkxdPv+u19bEViK/G80wtay+fiH06j3oA8y0QJBVAEDOYRwoyQIGeodUS1DIYiiVcuj30DC7XwtMRWILYCzz+oQcV8ylqTWJgwCXLFCBskHpEkLCEKYBVYkKtAZJBi+BbdclWHB0k8SJ7/xF4Ie5A7DfJk4+kgQU6AAPPIZUYogAGVWKsRw6JQC9Vu1c1XDfMgiAfBu86fRKjvjicXVP0m1AUFODNKgHnEEmC6DAIcwIBKiKsEAx1yeZsW3jjAFhJbyPP26IcwH08pYMeMewQJMGAQXeJ/BjNCAgRZoDawXGhNB1W3a8Eta9gCYgv4yXv0sTI+2q0dozQZFMEBMA+aGGW2PgHmwRFgQAZnYBUU95bs4Ft09a1DTJCYIB84r4dc25NlO9apdswECwQWYLY+YWwhgQwIDEKMkLEZJcD8lQFxP7NVSYxQhmwoloodlw+TfauuubXFBIgJ8Oz9CxjamzCtgWdAYJTpEmC2iAALMH9jDIi/EuKvAhCCQOSUHWxjQBIS2MaAGMP8HQFmy4kugw1aKdLAoF3cHBZfWzFOYpy804GiUZtPxRSKyRlJENh6xCgBBhnTZYMNCDkKVRArk2sQY8qucpCoqjJEEUQUpkuMUAAzKgSwGWUeNLFWhioDwyGFtDyu7NymNb8x4yDGyXs8c2daPbMJ2YQIFhAYZSZEgHmAWUuggEk4GCPHsjQOIgdIlShWVIRVic4Ml3lWmejQk1aLohSxXke1gqBIDJlYCGdwAgUwIIFlMGAeHIHoyuAMKYGTSO1FWnL5EsZBjMMX3/ju5nEX/HJv6pMCikBghAQ2W06AAGPAhlyWoKBCQFAiBBhOuKiZyWvkuQO94YADZnDIy+ey/8FT6Kxu8/NfL+Gi797HrbevYk1jDWU0ZQdUxFyLhExwFHYixEJUpktIdBkxymaLSGBDTlBWSo1G/uHJz/nzUe88ucVmiM1Ytf/zw+Q7V+xF3069FNGUpSAAGSSwmTDzAAVnkigKO1XENW3o5JLd55uTXtrkMVNnstO0Pdl55mOY1PN0qO0KFGxYAm5m+aqrvGjJH/2XO28INy5dzPcvrbjtZuid3qBpQWWShTFIIDPCIMBsAYMCpESOlULlNavr4aYpt//EbILYDO+w30zUN5tiiikQoQAEmC0jRhiMnVKFiUWQU1w50M51tX34s2bFM05+ETtPfQGwJxDYMgZu5r50cX7Dm74XLv/t4tyY1Axq1NyICEOIkC2MEAYDAhnMOBlMVwCMiyHUzmJw+SKtun4JmyA2YWjufo2m6/tQ74vkfsiCnEGBiRNrGWflIJAJrVK0KZndP8R7X7szhz31JGbMPBLoZesaZuEt5/G9X57DFy+6j0X3NemfUaPeMbUAnQSmS4AAM2ESI0KGtEKklKD1Zy3+fZuNEJvguU/dHWk6oWFyE1yADWILCATkTI4FkBXaneTBoVLPekYf577lKHp3PBGYxroMiK1pBYODX8kvedt54bd/HKx6+orC9UAhRihApstMmAQ2KINboGHhfJ/uuGohGyE2wnOf20so9iIjsCDwdwSYcRAIMF0GBVxaudHJ8bB9arzxDU/nwP3eDMxhCxhIgBgVGbc7uPb3n+EzX7yS39w2zIBrFIXBAoyZGNElwICBDEZAhcKNWvjDFhsgNsLzXrgbhOnYBgkMCDDjZkYFQU4Qa9CpQtkfXDvt+dM45vgToPZSJqIzDPfeC7ffAcuGIE6G2bNg9x1hSh9/z4DYBEP+Pl8993N84sJlDAw1kpUjNgqQzQgxfgJMlwGBENIKLbjwVjZAbIBnH9IkTt4HCoHBZvwMCBAPMIQgUjBTM3zw6H6OPuZfoedwNsfDcOFv4Io/wNW/g4W3Q7sFClBmyAlSC3oEvZNhj33hMY+Fpz0OnrEf7DiNTcuQyp9z3hWn8eGzV7B6ddMUlWQBxgYFsJk4MSqJNPBnLb5siDHEBnj2U3YlTNsR6obMhElgIARhmxBxVUmTYoePvGSGj3j5R+X6AQQ2bngAvvJdOPN7sGIF1MA5IxuKAEEQAgjIGZygNJSQqkzsbcCsR8HxL4VXHAZTpgBio+w/cMEv3sF7P7WEwU4dNYEEtkFggwAzAQEwuBPwmnt05xWLGEOMUc3drxZp7AvTC2gABszECLIhCIrglAtF3Zf492Om88rXnwM9e7BRFZz5FdIXzyMuGwQC1OsgQ8ogugwSIySwwUDOkDOEAhSgSuTcJuwyC97wEjj2KOjpZ4Nsum7nW+e+kfefuaysZhQ1IqRMlxEGxIQERoSOnFd2lAb/rMV/qFiHGMOzHzuDYspcmGSoi5yZEDPKNkFyBA2W5iXz4T/P+DjEZ7Exq5fA6z4EV14DfU1whpwBgQQ2YLAZYUACDBIgkACBWMs4GynA/o+DM/8VZs9kE37FG9/wFl90K2pMBgVIBhuCGD8DERCopMOA6gNLF2rZn+9jHWIM73HIPqRpvVggIBswCDCbYCDwAAE27bYp24kfnXkoT3ncGWzM1b+BEz8By5bjkJEMOYENCmCDzd/YgBghwGaUQIDECBkM1HrInUzYcTp84l34mQciNuLa69/Cc0/8v9SmNGnUTHYmZ4hR2IybAhhwAnWCvXogLLrqBtYh1nHmye9uvOnbVz2GWg1ij7ifDWLzRJfAQIzChqo0K1tV+/lPqjf+z0e/C8xhQ86/AN7+WUpFanSADNkgwHSZv2MwRhIjzCixlgExQgIEQTiLlAPFpH447c3wsmezEQt8+OuP0TUL250pU1WnEiMENuOmAM6QE1Qd0WjyX8c88brXfeSDFWuJdXjuPjNh7hyKeiZZOACZ8RPYIEE9Ug12VEzvX81ln34102edyhgG9LOfwfGnQwJiACpIBgkwW8b8PYEECEIguUZ0BZ87FR/xTMQ6DAhYfvfpHHL8N6rVTC5qdZErYxvJgBg3AwJnHDqSV92hO66+l7XEOjzroL0opk4ilZlQFwqAGRfzVwYrx0xYkzp8/oQ+XvaS72NmIv7eynvhya/GZUIhQ67AAmdQwGSEMCAeDIMEBLChCOTShCl98JOvwKyZbMBK/vu853DimW0mT6qjYCyRswlifMyoAE5Qb4tOe7UWXX4Ta4m1PO/1QvnxaGWkqgwWioyPeIAMWXmw5RCrAa4/7/3M3PGfGKMiURxxElz7R9woEAkyXQIZzFZkQIBBAoRDRO0EB+wN538Wih7Ws2LNV9jreR8l9k+ip24yxoYQBAazaQIMSIBBFl5VkYrrdefPM11iLc87ode5vY/CMjBdBsS4SGAgBHG/smOWD5fli58yuXbuRy4G+hnr89+G0z8PjQLcAQMGBAbENiRBNtSbMNyG9xwLb34d68stDj/hBVx548py+jRqcgAxYRJg4ySohNs3aNFlg3SJtTzv8JlIc3DNdkCYcZMgA0EiQrW65WKHKS1++OFXsMejT2WsVSvgGcfipctRzGCDBAgw25a5nxWQBEnkWTMJl34dJtUZKy9c8JHwzLd+PbfajdDTiORsbCZEAgNKgo6gWqiFP7+XLrGW5x26G9YO0DBEQIyPQRFsY4toMZgqTn6p+bfjvgg8ibG+9A047SxoNKAqwXRlQGCDxDYnMSLUIGX4t7fAG49mA67jtP98LedcVNCsR6ps7udsgoQZB9FlyEJVJFRLdPvPFtIl1vKcw/cB+gm1jAMTogDYgCDBUCvzpdN34MiDfgjUWYeHhtBzj4c7FkMMkBMjnAGxfZhRgiBwHR41Ey75MkxqMEaLH/zsKE748H00+yPZGRDYSGJcBMjIIleRXK3W4ktvpEusNTTrsMc36406hIwDODN+ostgkUtTVebic57I/rt/kbEu/SW85l+hGRiREwgwIMBsJwYEIYIilAnOOg1eeCjrue7P/8yzjruS/h0LhLEFGEmMhwQZkCGn0LE7jcU//j1detO5p+vxV98Vj7/4nsczWAYCGQlsxk9gg4AygTqZ3373tew85a2Mdfp/wGfPht7J2ELioWGDAg4BUkLDA/Avx8AHTmEs37vik3rci75JbaoIGQiAjUSX2AwjMEiGyqHqyenrx+55/bH/dkalUy/9TrH/L5bV//Eb5+9LK0NRt4MQBsT4CLAhqOoM5mJKB35/2VdpsB/rMKAXvx4uvw6aTVDBCAHmIRBAgBMMDcATHgOXnMt6Sv+O/V9+HPeuLmjWjILBjJ8YkRNkVPXU/d8nPPXPf3nMLh219vhurd5ZUrMv3jeETJchABkkMOMgEGBDeW+H/SdN5Uc//TkQWFfqwPyDoIxQFJDFCImHhAKjMuQKahX88dfQjIyRecH7DuXX1w4xKQRExmZiDARQFuoxaf8bB9i7I0/7UAMW1Wgu3osiCYJBgAGxSRJkgwIEyamTNXxLhxOPfiynf+xbjHX7baQDX0TsnQwKYIENQWC2LxtCYETOYEN7JVxxEczfjfWc+p1XcNY3b2QSdbuybDBdBsSmmRGOkLOI2Qw/+iY0ty0/+sie7FALKe9JhSAZCRCbJYFtJKGAVVkr7zTnfuYZPP/QTzPWjy6jOvZtFL39OGVElw0hgM12pwAYcoYQYHAlfPtsOOxprOfHvzqFN370B9TCFDtbxtiMn0ERFLEqE9ItqhUdpX2e3UNFLZTNvTDgbBATogAhUkXnYlUa4Bf/8Sr2mfN+xjr3QtI7TiP29IENNiNCAJvtRoABBbABQ4gwPABf+CQc+QzW85ffn8rz3v8tyNMMKNtMlAAjq8z0pptVxFJ538N6yLWaOsWeZETGTJQChAC1nBmurfKln3yZ9przEQj8VQbCBZfA8e+Gnn6QwYwKAWy2GwmcgQgYyICgNQTf+gI864ms5093vKtz2Enn13uYDgLbjJsYZcBCVaa3uoVIKe95ZANqNcrOXlRZEIwMiPEThICprA4tTj/uqRz74i8x1g03wkEvh/6pEDJkQEAIYLPdSJAzKAAGG5ygPQC//DHstQvr+drPXs+b338F06ZNtZTlDIjxEesQCpl6vhmplGd+oE5cXSPetDfKAgwyWCA2z0CAIJw6VmdFm2fvN5tvnPNjxqhabYr5h4JrUANyBgQBMNuPBDYgwECGqg0ehluug94a60pAfO0/P4+Lfr2Y/p17UDSI8RNIYMBJuJbpzLmJNKWUH/2VOvnmWHV+t29RFAKZbGOLcRNI4CTaaxK71IKvueQqKUTWYUBPOAIWLYW+AhAPLTHCJbndIUyN8JffsAEdDnjWISzqdOidVMORLiOBGQeBBQKUNCxymHrwjbWhOaVO/9rXi33+dEvxkvOvewwtRMhGGFuMl8QIZ5Fypt4uuf7Cb9Lf+1jWkYFw9PHwyz+QmoFY1MAGAWY7E0iAoapwu42eujdceC7raQ3+gT1f8Bqq3kgjioSATJAwm2XAFsFAjkqNyv91zNP+dMdjd6n0qp99Vc+8+LZ43Nd/vh9FTyDWjAJkgxgfiS6TsyBAGq648hvHM3fmmxjr1I/DOedBbw9IkA2my2w3EhiQQCJXJawZJJz8T/DBU1jPojs+3XnyG75Wq/cVCgYDNoQANuMjSB0wolHPXzjpgOtPeOdplVirnHfEfkUMdXI2DuDM+AmwEYIgOkOZ73/uqRy4138w1sWXwwnvwbUC2ZDNKLP9CGwIAWIgddrEsoRzPwWH/QPrufLaE6qjTrkm9PbF4GzuZ0MIYDMuBmQghUqhU1v4o9/TJdby3OfsDWESFEZkjJgoCUIQZafiY++awjHPvASos67BFrzg9VS3LqRo1CEZTJfZrgyEAIVgcAh23wMu/Sr0Nhij4vyfPIeTPrWq7OkNtVyZvxJgxsGAAINTgGqNFv30RrrEWmm3g+YFN3ci1wwxAwIxbsYEiSBRpopXHFHxiRO/DuzPWN84H075MNQbQAALDASBzbYlwGAgiqxM6FT4A/8bHXcUY2X4fTj97NfypR/XqhBUpDIDAjNuEtggg1KEtEQLLl1Il1grzztshtTcFdtYGRDjZkYJO1MRXItxmKvOejU77fxexhoYhmf8I9y7khwDAYEFEthsUwYkUCA5oVwRdnkU/PhcmNpkPXff8yEOOem8VFYN2TkEM8qAGB8BAiqgE6x0e1j4/5bSJdaq5r6hGaV9zb0SZIyYEIPBEsqGFauq6tlP7C++f8ZPgD7G+vR/wcfOwj11hCEnIDDKbH0CDAYEhEDliqKT4YPvgeOPAgNiXWt47olH8publtFbbxCiURAyIMZPgMGVURlQdYMW/mKQLrEOz3vN/mhZgck4CAyI8TEgCMGQRZmh3VrNdd96H7vs9GrWk+CoN8P1N1ClkiIGyAYbFACz9YhRBoPpatTRyhYcfDB892MQWd+yu77q2Ud+1DMe1RuwUDC2kBgXAaZLQAA6wHAitK/XgiszXWId3uWQ+ah/KqGWwWKiBFjGDkSZoYGKD/9LP8e/9CJgKmMtXQWHvBTaCZxAhpyBAALMWmbLCTCYURLUCzoDbeo9U+CKL8OjdmasBMvjN7/9Yt5y1qo0eXoRUzJY5AwS4ydQAANuibx6tRZfeRNribXueeoBmnlPe3rVmrFb0WhmFIUFzkyMIWewcagHTetbwS/OPIGpO7yLDfnhT+CkfwfVSO4QgyBlMCDxADNxAgMCbAzkoiB2AmTD194Nhz+XDcqrP8Y+x33FnTwpp8oxYDA4gwRmMwwEMCCRyYRUiubSO3TLb+9lLbGOL7/r7bXXXnTD4+KgBAIFyAYxAQYbCMaINaur1tMfV+u54HPfA2azIedfDO/+LCThPIRCgmRAjLBAdJlxMWuJETJEQSxgKEGtCWe+CY48go1YyLNP/Cd+e9Maeht1h2CFIEYYEOMjsIAMZVa7v8jnHD7/Tyd//lNt1hJjeNZT9qHW30cu7FBDZgLMKIGCIUEiMDwwzE/Ofj4H7PVxNuYXV8A7z4ClSwADFc5GKiCbv5H4KzNKbIAEztgZh4hDIA6ZvNMMwuffAwc/iY265nfv7Bxy/I/qM2f14gwEg4XExAgQuA1lR8T2oBZfcQPrEGN49uN2qvpmzontOnI0tpgoCQzkDLUAZWmev1/gnNM/CfWnszGLFsLbPwO/uRoIECNQAQFSBhsUGGHzdyRAgEGARM4JZHKrRUgmPONpcNZpsOMMNsaUv9S/vPdtXPh7Ue81KTMiZ5CYmAAyqAN5ULi9UIuuvo91iDE8e79I0dwXT23gegYEZmLMCAM29PYFYqfNu4/cgde++kuosRsbk0v8uW+ic8+Hu+7DsYZCAJlcdRAgBAhsRhkkQGDjEMkZlEqcSuKc3eGEY+DYF0Kss3Htv/CD/34rb/3mPcRJBVXHjDDYIMBMgAADHdmDnUqrb6jf8YeSdYgN8OxD5hH6ZuBaRhKYiTMYLFmhEJLoo8O/vmyWX/S8M9Q7eU+IbNSylfCNS+Cr34Z77wMCpAoCIEEQYEZkQzakjHMJGPX0wPw94Y2vgaP+AfrrbJzBg3/igh+cUp1y7sJioN5LyEY29xMTJ7oEpqste/U9YdGv72QMsQGe86wGofkYHIQBmS1jCDIZEerGKbBTo827XjKTo19wKrW+p4PYpDUd+Omv4dKfwp9uhMV3w6oBCDWwQIKcoV5Afy9p5jTi454Ihx4Mz3syTGqyWV5zOd/5zgfTBy64J67IDaKAZCSwQQbExAgkugRVJq38s+68osUYYiPyrkfuIYdpGONsZDFRYpSBQFcByVRTIsUpL+7l1S95O2oezUSUGRYvwYvuQitWQKMPHj0Pz5mBajUmrvM9n3vWmf7QT5aHNY0aMRkSWCABGRATI0CgILBQWqoFF93OBoiN8LwX9gHzcSoggAIgwIww42BGCQTYEOuirHBtKOkZT47882v+gQP3fRswh+3rdq7/4+f42NmX5csWlIF6DSUgATKYvxFgxkGMMmCQhVyBb9KCHw+xAWITPOfJuxLjDtAL9IAjmC4zfgYEAgwEMcJSarccU2c4P/nxk8LX33cU06f/L2A629Zy2nf/Z3XEaRcXv7txVa73NELRAyLgZCRAgNkiBlRBaANtyF6iO668k40Qm+BZT6ih9FiKyQGa4DogtowBgVhL4AxZmeF28rR6S+84YWeOfvqx7Dz15UCTrWuQVSu+x7k//ELntG/dE131xr5GZEQSBGNGGRAgwGyeAAsMKAJDoDWiHCoTuqG467oOGyE2w7MP3plU7EJRh9AwSNhMnEFAFgRAAbIFGAk67Zw7rRwireqJT5hVnP2BI5k7+Qhgd3AEsQUM3Maawe/zitMu5pqrF5ehEWuNehNFowzZEIKwjekyEyaBBQgQuCU6qyG3F+m+3y5hE8RmDDz20Nh7bzlfsdlPvQkhQAac2HJmlEACyUgiJ1PZDLQrdt6x5PhXN9ln+i7s1L8382bty7SpB1GLe7FxmZQW0F79K+5Y8iduuvuP/OHeO6vPfb9TlEsjffXoTFSMAQzIGLCFZDBbJoAFAmNUdkTP4Gp6pt6iGy7MbIIYh5987KPNZ3758n1qa4KICUuQM5LYMgYbQjAZCEFgkREhJGwoOxCLjCqocqCQaU4JzJ0/lQP2m8tBu+7Mo2fWaVUtrrp2KT//6RJuXriMlRom1xJZcpUoFYq6K4g1EQM4QwjCBtvYYECABDYTJ0xAOUGFOv1F+s7R+//lVZ84bYjNEOOUdzhwlnr6HoWEQw+yQOLBM9ggAQIEIRgcUJYNygGMSQHKmBAdGqFFQzG1s2O5PBLahWNfUKoVKMSMIWDljGIEbBDYdAkJsLFBYssIELZRFJSD0Emip75Yiy65m3EQE+A5B++BmQZ9RjVQAMyDZgOGEMAYSSDAoCBMl4yzsSDKZIMEAUEW2UZRODFCoksEmWzAYCBnQCBAApstJ0CYgHILGBRVtTQRFxb3XG7GQUxAnvvkKA/uQ5zaIE81SIwIgNlqbEaILjFCAgJIIBlngcl0GSQjAzYgRhkQYEYJY4QwRmwpAQYJEFQZwkLhPERq3qS7r6kYJzFB3n9eIxXeO6yYUaizE0hABMzWYzAgwJi/kbifALGWGGHWMliAGWVAEOgS2Gw1AjJQLCNPWtwORbhV1y4YZgLEFshP2r1XK+KedHaIhEngBtucAbGWAbEhxghhhLif2foEMjiBV0N9Scm0dIuuuX2ICRJbyPP2mISLPdCkiKcDDbBABsz/LAIMCCwQXR3wcmBlSSxv14Lb17AFxIPgeXv3odp8u7cQU8E9gIAEGBBgEGAemSQwI2whutSGsEow0MHt27Tw5gG2kHiQPP/AJplHU3VqKfQRPRkUwXQJTJd5RBFgMSoDAgVQhrwaipYIbiFu1c1XD/MgiK3A8w+qUXb2zGXRpNHrQDS5Lu6XxQjxyCGBARsUMRmFDO01oCxqHqSebtPNv27zIImtpNrl/YHEblEXTyO0TH0eZAGGbBCPIAIEiBHuQGsRVDW555lLpWmLdNepia1AbGWtXfea0VCcXZbTQy1OBgLGyHSJR44MEcgZ1BL1hVXHcxc3bv7FfWxFYhv4/FtObpx4wTW7MRz6aEAV6hS5BxwBM0KAefgQYAECElYLpWFIdRFaa752xP4LXn/2WW22MrGNtHc/WPUVq2fSy84OvVGaAQoGC5v1CTDbnARmHWaUAIEEHsYalNqDFblxz0BvfcmkBb8024DYxrzLE3qgMYti2hRUD2TAGQSIURLYjLDZLAFm8ySweYD4GxkQDxAQBcZ5OCsMLKdcdbfu+mObbUhsJ573giaKu+BqMpkAdSxbMYMzGHAAA6JLQAbERklgsz4xQmaExQgbBCahYHAAi1E5gDKKy3F5j+748TDbgdjOPPvAJq7tjKZMSUU9xtwWORlqoBqEDGRwBMyoDBKjDKZLgBllRgkQCLBBdAVwAGXIAgeIQGoLyZCFc4bh5VAu0Z1XDrMdiYdIucvhtaLRnGaGdjCxt2ytVqM2Q9AwoQUuoUqgJoTSCFAUKYMBiVECCyTsjKJBmJTAAZSAiNWHcua/mAMAAAESSURBVJvkQWIjKSmkwPQWneHlCuVSLbgk8RAQDwOfePvbet5x2YXTuK8xlXxIs0Kh0NUBlSbPgNDI2CIFowzBQBSKjEgCZQiACkgtoBLKoJWCSK4OJNSHM9UlrZXzvPzMQ165/H0f+USLh5h4mPHsV0ezQ6/jr5ohpd6Kvp7cmNKo9xE7y/rkWLqhZEYkuoQCtArKeo1a7yCUyR1yqpet0l7dVvBQWR46WKu3hrTg7MTDiHiEOOfT76j1/SUUL76kNzSHl0PPguDaUivUod1vVj0Jduj3V45a5XZcWR1x2cJqzpUXmoc58QhyxhlfK978iVrU8EIx5YpAczlQg6FJsOrQzLQd88dPST7lhGNLHiH+P2W5wJRJ4XWfAAAAAElFTkSuQmCC'

        plugin.realtimeTracker.icon = L.Icon.Default.extend({
            options: {
                iconUrl: iconImage,
                iconRetinaUrl: iconImage2x,
                iconSize: [32, 32],
                iconAnchor: [16, 16]
            }
        });


        plugin.realtimeTracker.drawnTraces = new L.LayerGroup()
        // to avoid any favouritism, we'll put the player's own faction layer first
        window.addLayerGroup('Realtime Tracker', plugin.realtimeTracker.drawnTraces, true);
        map.on('layeradd', function (obj) {
            if (obj.layer === plugin.realtimeTracker.drawnTraces) {
                obj.layer.eachLayer(function (marker) {
                    if (marker._icon) window.setupTooltips($(marker._icon));
                });
            }
        });

        $("<style>").prop("type", "text/css").html('' +
            '.plugin-realtime-tracker-names{' +
            'color:#FFFFBB;' +
            'font-size:11px;line-height:12px;' +
            'text-align:center;padding: 2px;' // padding needed so shadow doesn't clip
            +
            'overflow:hidden;'
            // could try this if one-line names are used
            //    +'white-space: nowrap;text-overflow:ellipsis;'
            +
            'text-shadow:1px 1px #000,1px -1px #000,-1px 1px #000,-1px -1px #000, 0 0 5px #000;' +
            'pointer-events:none;' +
            '}'
        ).appendTo("head");
        window.plugin.realtimeTracker.labelLayerGroup = new L.LayerGroup();
        window.addLayerGroup('Realtime Tracker Names', window.plugin.realtimeTracker.labelLayerGroup, true);

        plugin.realtimeTracker.playerPopup = new L.Popup({
            offset: [0, -8]
        });

        // addHook('publicChatDataAvailable', window.plugin.realtimeTracker.handleData);
        if (window.FOLLOW_GLOBAL_REFRESH_TIMEOUT) {
            window.requests.addRefreshFunction(window.plugin.realtimeTracker.requestServer)
        } else {
            setTimeout(window.plugin.realtimeTracker.requestServer, 1000)
        }

        // window.map.on('zoomend', function () {
        //     window.plugin.realtimeTracker.zoomListener();
        // });
        // window.plugin.realtimeTracker.zoomListener();

        plugin.realtimeTracker.setupUserSearch();
    }

    window.plugin.realtimeTracker.stored = {};

    plugin.realtimeTracker.onClickListener = function (event) {
        var marker = event.target;

        if (marker.options.desc) {
            plugin.realtimeTracker.playerPopup.setContent(marker.options.desc);
            plugin.realtimeTracker.playerPopup.setLatLng(marker.getLatLng());
            map.openPopup(plugin.realtimeTracker.playerPopup);
        }
    };

    // force close all open tooltips before markers are cleared
    window.plugin.realtimeTracker.closeIconTooltips = function () {
        plugin.realtimeTracker.drawnTraces.eachLayer(function (layer) {
            if ($(layer._icon)) {
                $(layer._icon).tooltip('close');
            }
        });
    }

    /*
    window.plugin.realtimeTracker.zoomListener = function () {
        var ctrl = $('.leaflet-control-layers-selector + span:contains("Realtime Tracker")').parent();
        //if (window.map.getZoom() < window.REALTIME_TRACKER_MIN_ZOOM) {
        //    if (!window.isTouchDevice()) plugin.realtimeTracker.closeIconTooltips();
        //    plugin.realtimeTracker.drawnTraces.clearLayers();
        //    ctrl.addClass('disabled').attr('title', 'Zoom in to show those.');
        //    //note: zoomListener is also called at init time to set up things, so we only need to do this in here
        //    // window.chat.backgroundChannelData('plugin.realtimeTracker', 'all', false);   //disable this plugin's interest in 'all' COMM
        //} else {
            ctrl.removeClass('disabled').attr('title', '');
            //note: zoomListener is also called at init time to set up things, so we only need to do this in here
            // window.chat.backgroundChannelData('plugin.realtimeTracker', 'all', true);    //enable this plugin's interest in 'all' COMM
        //}
    }
    */

    window.plugin.realtimeTracker.getLimit = function () {
        return new Date().getTime() - window.REALTIME_TRACKER_MAX_TIME;
    }

    window.plugin.realtimeTracker.discardOldData = function () {
        var limit = plugin.realtimeTracker.getLimit();
        $.each(plugin.realtimeTracker.stored, function (plrname, player) {
            var i;
            var ev = player.events;
            for (i = 0; i < ev.length; i++) {
                if (ev[i].time >= limit) break;
            }
            if (i === 0) return true;
            if (i === ev.length) return delete plugin.realtimeTracker.stored[plrname];
            plugin.realtimeTracker.stored[plrname].events.splice(0, i);
        });
    }

    window.plugin.realtimeTracker.processNewData = function (data) {
        var limit = plugin.realtimeTracker.getLimit();
        $.each(data.result, function (ind, json) {
            // skip old data
            if (json['last_updated'] < limit) return true;

            // find player and portal information
            var plrname, uid, lat, long

            plrname = json['agent_name']
            uid = json['uid']
            lat = json['lat']
            long = json['long']

            // skip unusable events
            if (!plrname || !lat || !long || !uid) return true;

            var newEvent = {
                lat,
                long,
                time: json['last_updated'],
            };

            var playerData = window.plugin.realtimeTracker.stored[plrname];

            // short-path if this is a new player
            if (!playerData || playerData.events.length === 0) {
                plugin.realtimeTracker.stored[plrname] = {
                    nick: plrname,
                    uid,
                    events: [newEvent]
                };
                return true;
            }

            var evts = playerData.events;
            // there’s some data already. Need to find correct place to insert.

            var last_evt = evts[evts.length - 1]
            if (last_evt.lat === lat && last_evt.long === long) {
                evts[evts.length - 1].time = json['last_updated']
                return true
            }

            // so we have an event that happened at the same time. Most likely
            // this is multiple resos destroyed at the same time.

            // user didn't update any new move.
            if (evts.some(evt => evt.time >= json['last_updated'])) return true


            // the time changed. Is the player still at the same location?

            // assume this is an older event at the same location. Then we need
            // to look at the next item in the event list. If this event is the
            // newest one, there may not be a newer event so check for that. If
            // it really is an older event at the same location, then skip it.


            // if it’s the same location, just update the timestamp. Otherwise
            // push as new event.
            evts.push(newEvent)

            // update player data
            plugin.realtimeTracker.stored[plrname].events = evts;
        });
    }

    window.plugin.realtimeTracker.getLatLngFromEvent = function (ev) {
        //TODO? add weight to certain events, or otherwise prefer them, to give better locations

        return L.latLng(ev.lat, ev.long);
    }

    window.plugin.realtimeTracker.ago = function (time, now) {
        var s = (now - time) / 1000;
        var h = Math.floor(s / 3600);
        var m = Math.floor((s % 3600) / 60);
        var returnVal = m + 'm';
        if (h > 0) {
            returnVal = h + 'h' + returnVal;
        }
        return returnVal;
    }

    window.plugin.realtimeTracker.removeLabel = function (guid) {
        var previousLayer = window.plugin.realtimeTracker.labelLayers[guid];
        if (previousLayer) {
            window.plugin.realtimeTracker.labelLayerGroup.removeLayer(previousLayer);
            delete plugin.realtimeTracker.labelLayers[guid];
        }
    }

    window.plugin.realtimeTracker.clearAllPortalLabels = function () {
        for (var guid in window.plugin.realtimeTracker.labelLayers) {
            window.plugin.realtimeTracker.removeLabel(guid);
        }
    }

    window.plugin.realtimeTracker.drawData = function () {
        var isTouchDev = window.isTouchDevice();

        var gllfe = plugin.realtimeTracker.getLatLngFromEvent;

        window.plugin.realtimeTracker.clearAllPortalLabels();

        var polyLineByAge = [
            [],
            [],
            [],
            []
        ];

        var split = REALTIME_TRACKER_MAX_TIME / 4;
        var now = new Date().getTime();
        $.each(plugin.realtimeTracker.stored, function (plrname, playerData) {
            if (!playerData || playerData.events.length === 0) {
                console.warn('broken player data for plrname=' + plrname);
                return true;
            }

            // gather line data and put them in buckets so we can color them by
            // their age
            var playerLine = [];
            for (var i = 1; i < playerData.events.length; i++) {
                var p = playerData.events[i];
                var ageBucket = Math.min(parseInt((now - p.time) / split), 4 - 1);
                var line = [gllfe(p), gllfe(playerData.events[i - 1])];

                polyLineByAge[ageBucket].push(line);
            }

            var evtsLength = playerData.events.length;
            var last = playerData.events[evtsLength - 1];
            var ago = plugin.realtimeTracker.ago;

            // tooltip for marker - no HTML - and not shown on touchscreen devices
            var tooltip = playerData.nick + ', ' + ago(last.time, now) + ' ago';

            // popup for marker
            var popup = $('<div>')
                .addClass('plugin-realtime-tracker-popup');
            $('<span>')
                .addClass('nickname')
                .css('font-weight', 'bold')
                .css('color', 'white')
                .text(playerData.nick)
                .appendTo(popup);

            popup
                .append('<br>')
                .append(document.createTextNode(ago(last.time, now)))
                .append('<br>')
                .append(`UID: ${playerData.uid}`)
                .append('<br>')
                .append(`${last.lat},${last.long}`)

            // show previous data in popup

            // marker opacity
            var relOpacity = 1 - (now - last.time) / window.REALTIME_TRACKER_MAX_TIME
            var absOpacity = window.REALTIME_TRACKER_MIN_OPACITY + (1 - window.REALTIME_TRACKER_MIN_OPACITY) * relOpacity;

            // marker itself
            var icon = new plugin.realtimeTracker.icon();
            // as per OverlappingMarkerSpiderfier docs, click events (popups, etc) must be handled via it rather than the standard
            // marker click events. so store the popup text in the options, then display it in the oms click handler
            var m = L.marker(gllfe(last), {
                icon: icon,
                opacity: absOpacity,
                desc: popup[0],
                title: tooltip
            });
            m.addEventListener('spiderfiedclick', plugin.realtimeTracker.onClickListener);

            var guid = plrname;
            var previousLayer = window.plugin.realtimeTracker.labelLayers[guid];
            var label = L.marker(gllfe(last), {
                icon: L.divIcon({
                    className: 'plugin-realtime-tracker-names',
                    iconAnchor: [window.plugin.realtimeTracker.NAME_WIDTH / 2, -12],
                    iconSize: [window.plugin.realtimeTracker.NAME_WIDTH, window.plugin.realtimeTracker.NAME_HEIGHT],
                    html: plrname
                }),
                guid: guid,
            });
            window.plugin.realtimeTracker.labelLayers[guid] = label;

            label.addTo(window.plugin.realtimeTracker.labelLayerGroup);

            // m.bindPopup(title);

            if (tooltip) {
                // ensure tooltips are closed, sometimes they linger
                m.on('mouseout', function () {
                    $(this._icon).tooltip('close');
                });
            }

            playerData.marker = m;

            m.addTo(plugin.realtimeTracker.drawnTraces);
            window.registerMarkerForOMS(m);

            // jQueryUI doesn’t automatically notice the new markers
            if (!isTouchDev) {
                window.setupTooltips($(m._icon));
            }
        });

        // draw the poly lines to the map
        $.each(polyLineByAge, function (i, polyLine) {
            if (polyLine.length === 0) return true;

            var opts = {
                weight: 2 - 0.25 * i,
                color: REALTIME_TRACKER_LINE_COLOUR,
                clickable: false,
                opacity: 1 - 0.2 * i,
                dashArray: "5,8"
            };

            $.each(polyLine, function (ind, poly) {
                L.polyline(poly, opts).addTo(plugin.realtimeTracker.drawnTraces);
            });
        });

    }

    window.plugin.realtimeTracker.requestServer = function () {
        // if (window.map.getZoom() < window.REALTIME_TRACKER_MIN_ZOOM) return;
        if (window.isIdle() && !window.FOLLOW_GLOBAL_REFRESH_TIMEOUT) return setTimeout(window.plugin.realtimeTracker.requestServer, window.REALTIME_TRACKER_REQUEST_INTERVAL)
        let req = {
            url: "https://agent-realtime-tracker.jsw3286.eu.org/query",
            method: "POST",
            dataType: "json",
            data: {
                agent_name: window.PLAYER.nickname,
                priv_key: '%%%PRIVKEY%%%'
            },
            timeout: 4000
        }

        $.ajax(req).done((data) => {
            plugin.realtimeTracker.discardOldData();
            plugin.realtimeTracker.processNewData(data);
            if (!window.isTouchDevice()) plugin.realtimeTracker.closeIconTooltips();

            plugin.realtimeTracker.drawnTraces.clearLayers();
            plugin.realtimeTracker.drawData();
            if (!window.FOLLOW_GLOBAL_REFRESH_TIMEOUT) setTimeout(window.plugin.realtimeTracker.requestServer, window.REALTIME_TRACKER_REQUEST_INTERVAL)
        }).fail(() => {
            if (!window.FOLLOW_GLOBAL_REFRESH_TIMEOUT) setTimeout(window.plugin.realtimeTracker.requestServer, window.REALTIME_TRACKER_REQUEST_INTERVAL * 2)
        })
    }

    window.plugin.realtimeTracker.findUser = function (nick) {
        nick = nick.toLowerCase();
        var foundPlayerData = false;
        $.each(plugin.realtimeTracker.stored, function (plrname, playerData) {
            if (playerData.nick.toLowerCase() === nick) {
                foundPlayerData = playerData;
                return false;
            }
        });
        return foundPlayerData;
    }

    window.plugin.realtimeTracker.findUserPosition = function (nick) {
        var data = window.plugin.realtimeTracker.findUser(nick);
        if (!data) return false;

        var last = data.events[data.events.length - 1];
        return plugin.realtimeTracker.getLatLngFromEvent(last);
    }

    window.plugin.realtimeTracker.centerMapOnUser = function (nick) {
        var data = plugin.realtimeTracker.findUser(nick);
        if (!data) return false;

        var last = data.events[data.events.length - 1];
        var position = plugin.realtimeTracker.getLatLngFromEvent(last);

        if (window.isSmartphone()) window.show('map');
        window.map.setView(position, map.getZoom());

        if (data.marker) {
            window.plugin.realtimeTracker.onClickListener({
                target: data.marker
            });
        }
        return true;
    }

    window.plugin.realtimeTracker.onNicknameClicked = function (info) {
        if (info.event.ctrlKey || info.event.metaKey) {
            return !plugin.realtimeTracker.centerMapOnUser(info.nickname);
        }
        return true; // don't interrupt hook
    }

    window.plugin.realtimeTracker.onSearchResultSelected = function (result, event) {
        event.stopPropagation(); // prevent chat from handling the click

        if (window.isSmartphone()) window.show('map');

        // if the user moved since the search was started, check if we have a new set of data
        if (false === window.plugin.realtimeTracker.centerMapOnUser(result.nickname))
            map.setView(result.position);

        if (event.type == 'dblclick')
            map.setZoom(17);

        return true;
    };

    window.plugin.realtimeTracker.onSearch = function (query) {
        var term = query.term.toLowerCase();

        if (term.length && term[0] == '@') term = term.substr(1);

        $.each(plugin.realtimeTracker.stored, function (nick, data) {
            if (nick.toLowerCase().indexOf(term) === -1 && nick != data.uid.toString()) return;

            var event = data.events[data.events.length - 1];

            query.addResult({
                title: '<mark class="nickname help ' + TEAM_TO_CSS[getTeam(data)] + '">' + nick + '</mark>',
                nickname: nick,
                description: 'last seen ' + unixTimeToDateTimeString(event.time),
                position: plugin.realtimeTracker.getLatLngFromEvent(event),
                onSelected: window.plugin.realtimeTracker.onSearchResultSelected,
            });
        });
    }

    window.plugin.realtimeTracker.setupUserSearch = function () {
        addHook('nicknameClicked', window.plugin.realtimeTracker.onNicknameClicked);
        addHook('search', window.plugin.realtimeTracker.onSearch);
    }


    var setup = plugin.realtimeTracker.setup;

    // PLUGIN END //////////////////////////////////////////////////////////


    //setup.info = plugin_info; //add the script info data to the function as a property
    if (!window.bootPlugins) window.bootPlugins = [];
    window.bootPlugins.push(setup);
    // if IITC has already booted, immediately run the 'setup' function
    if (window.iitcLoaded && typeof setup === 'function') setup();
} // wrapper end
// inject code into site context
var script = document.createElement('script');
var info = {};
if (typeof GM_info !== 'undefined' && GM_info && GM_info.script) info.script = {
    version: GM_info.script.version,
    name: GM_info.script.name,
    description: GM_info.script.description
};
script.appendChild(document.createTextNode('(' + wrapper + ')(' + JSON.stringify(info) + ');'));
(document.body || document.head || document.documentElement).appendChild(script);
